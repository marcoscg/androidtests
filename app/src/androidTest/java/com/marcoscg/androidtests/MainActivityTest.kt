package com.marcoscg.androidtests

import junit.framework.TestCase.assertNotNull

import android.app.Activity
import android.widget.Button
import android.widget.EditText
import androidx.test.rule.ActivityTestRule
import org.junit.*
import org.junit.runner.*
import org.junit.runners.*

@RunWith(JUnit4::class)
class MainActivityTest {

    // https://proandroiddev.com/fix-kotlin-and-new-activitytestrule-the-rule-must-be-public-f0c5c583a865
    @Rule @JvmField
    var activityActivityTestRule = ActivityTestRule(MainActivity::class.java)

    private var activity: Activity? = null
    private var editText: EditText? = null
    private var button: Button? = null

    @Before
    fun init() {
        activity = activityActivityTestRule.activity

        editText = activity!!
            .findViewById(R.id.edit) as EditText
        button = activity!!
            .findViewById(R.id.save) as Button
    }

    /**
     * Test if your test fixture has been set up correctly.
     * You should always implement a test that
     * checks the correct setup of your test fixture.
     * If this tests fails all other tests are
     * likely to fail as well.
     */

    @Test
    fun testPreconditions() {
        assertNotNull("activity is null", activity)
        assertNotNull("editText is null", editText)
        assertNotNull("button is null", button)
    }
}
