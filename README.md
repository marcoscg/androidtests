# AndroidTests

An android demo project that demonstrates how to integrate bitbucket pipelines to perform unit and instrumental tests.

## 🔗 Contents

 1. [Enable pipeline in your repository](#markdown-header-enable-pipeline-in-your-repository).
 2. [Configure your android project for pipeline](#markdown-header-configure-your-android-project).
 3. [Configure the pipeline in your repo](#markdown-header-configure-pipeline).
 4. [Useful links](#markdown-header-useful-links).

---

### Enable pipeline in your repository

To enable pipelines in your repository, navigate to **_"Pipelines"_** in the left repo menu. Then, navigate to **_"Choose a language template"_** and select your project programming language (```Java (Gradle)``` for Android). A new file called "_[bitbucket-pipelines.yml](https://bitbucket.org/marcoscg/androidtests/src/master/bitbucket-pipelines.yml)_" will be added to the repo. This file is everything you need to configure the pipeline.

### Configure your android project

In order to prevent licenses agreement issues, you need to add one new line to the gradlew file. With that line, we are always accepting gradle licenses (which is mandatory to make gradle builds).

```
yes | sdkmanager --licenses
```

Add it in the top of the file (after ```#!/usr/bin/env sh```).

### Configure pipeline

The next (and final) step is to configure the pipeline. It is like configuring a docker container.

We are going to use the "android-build-box" image. With it, we will be able to execute unit and instrumental tests. Indicate it inside the "_bitbucket-pipelines.yml_" file:

```
image: mingc/android-build-box:latest

# Optional
options:
  size: 2x # just to make sure we have enough resources
```
‏‏‎

After that, we have to configure the image. We need an android emulator with a desired sdk version (sdk v28 in this example) in order to execute instrumental tests. This is the complete configuration (commented):

```
pipelines:
  default:
    - step:
        caches:
          - gradle
        script: 
          # set up necessities
          - sdkmanager "system-images;android-28;google_apis;x86"
          - sdkmanager --update
          - echo yes | sdkmanager --licenses
          - echo no | avdmanager create avd --force --name test_device --package "system-images;android-28;google_apis;x86" -c 100M
          - $ANDROID_HOME/emulator/emulator -no-window -no-audio -avd test_device -no-snapshot-load -no-snapshot-save &
          # build and execute unit tests while waiting for device to be completely started
          - bash ./gradlew build
          - bash ./gradlew assembleDebug
          # execute unit tests
          - bash ./gradlew test
          # make sure the device is completely started before proceed
          - while [ "`adb shell getprop init.svc.bootanim | tr -d '\r' `" != "stopped" ] ; do sleep 1 && adb shell getprop init.svc.bootanim | tr -d '\r' ; done
          # unlocking the screen
          - adb shell input keyevent 82
          # execute instrumental tests
          - bash ./gradlew connectedAndroidTest
```
‏‏‎

### Useful links
 - [Get started with Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html).
 - [Setup Android Continuous Integration (CI) for Bitbucket in 1 minute](https://medium.com/rocknnull/setup-android-continuous-integration-ci-for-bitbucket-in-1-minute-9b72a4f1d745).
 - [Install and Create Emulators using AVDMANAGER and SDKMANAGER](https://gist.github.com/mrk-han/66ac1a724456cadf1c93f4218c6060ae).
 - [Start the emulator from the command line (Google)](https://developer.android.com/studio/run/emulator-commandline).
 - [Extended tutorial](https://proandroiddev.com/bitbucket-pipelines-android-6eeff631f2eb).
 - [Some Bitbucket android pipelines](https://github.com/kigen/bitbucket-pipelines-android).